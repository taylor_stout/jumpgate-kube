package main

import (
	"compress/gzip"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

// RequestResponse houses data structure for requests and responses
type RequestResponse struct {
	JobID      string      `json:"jobid"`
	Type       string      `json:"reqtype"`
	SourceAuth interface{} `json:"srcauthkey"`
	SrcBucket  string      `json:"srcbucket"`
	SrcObject  string      `json:"srcobject"`
	DestBucket string      `json:"destbucket"`
	DestAuth   interface{} `json:"destauthkey"`
	DestObject string      `json:"destobject"`
	ChunkSize  int         `json:"chunksize"`
	Offset     int64       `json:"offset"`
}

func main() {

	http.HandleFunc("/", handle)
	log.Fatal(http.ListenAndServe(":8080", nil))

}

func handle(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Serving Request.")
	fmt.Fprintf(w, "GCS Input Plugin Req\n")

	ctx := context.Background()
	decoder := json.NewDecoder(r.Body)
	var req RequestResponse

	err := decoder.Decode(&req)
	if err != nil {
		panic(err)
	}

	sourceAuthJSON, err := json.Marshal(req.SourceAuth)
	if err != nil {
		panic(err)
	}

	destAuthJSON, err := json.Marshal(req.DestAuth)
	if err != nil {
		panic(err)
	}

	srcclient, err := storage.NewClient(ctx, option.WithCredentialsJSON(sourceAuthJSON))
	destclient, err := storage.NewClient(ctx, option.WithCredentialsJSON(destAuthJSON))
	defer srcclient.Close()
	defer destclient.Close()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Authenticated.")

	if req.Type == "Survey" {
		surv := survey(ctx, srcclient, destclient, req)
		//fmt.Println(surv)
		w.Write(surv)
	} else {
		workstream(ctx, srcclient, destclient, req)
	}
}

func workstream(ctx context.Context, srcclient *storage.Client, destclient *storage.Client, req RequestResponse) {
	fmt.Println("Workstream Request")
	srcobj := srcclient.Bucket(req.SrcBucket).Object(req.SrcObject)
	destobj := destclient.Bucket(req.DestBucket).Object(req.DestObject)

	srcReader, err := srcobj.NewRangeReader(ctx, req.Offset, int64(req.ChunkSize))
	defer srcReader.Close()
	if err != nil {
		log.Fatal(err)
	}

	destWriter := destobj.NewWriter(ctx)
	defer destWriter.Close()

	zw := gzip.NewWriter(destWriter)
	defer zw.Close()

	if _, err := io.Copy(zw, srcReader); err != nil {
		log.Fatal(err)
	}

}

func survey(ctx context.Context, srcclient *storage.Client, destclient *storage.Client, req RequestResponse) []byte {
	fmt.Println("Survey Request.")
	srcobj := srcclient.Bucket(req.SrcBucket).Object(req.SrcObject)
	objAttrs, err := srcobj.Attrs(ctx)
	if err != nil {
		//TODO: Handle Error
	}
	var workstreams []RequestResponse

	var offset int64 = 0
	iter := 1
	size := objAttrs.Size
	fmt.Println(size)
	for offset <= size {
		fmt.Println(offset)
		ws := RequestResponse{
			JobID:      req.JobID,
			Type:       "Workstream",
			SourceAuth: req.SourceAuth,
			SrcBucket:  req.SrcBucket,
			SrcObject:  req.SrcObject,
			DestBucket: req.DestBucket,
			DestObject: req.JobID + "-" + strconv.Itoa(iter),
			DestAuth:   req.DestAuth,
			Offset:     offset,
			ChunkSize:  req.ChunkSize}
		workstreams = append(workstreams, ws)
		offset += int64(req.ChunkSize)
		iter++
	}
	resp, _ := json.Marshal(workstreams)
	return resp
}
