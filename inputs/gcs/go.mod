module bitbucket.org/taylor_stout/jumpgate-kube/inputs

go 1.13

require (
	cloud.google.com/go/storage v1.1.2
	google.golang.org/api v0.11.0
)
